# Cypress TDC POA 2019

## Requirements: 

* [Node.js](http://nodejs.org/)
* [Npm](https://www.npmjs.com/)

## Start Application

### Install Dependencies

```
$ npm install
```

### Running Application

```
$ npm start
```

## Run tests

```
$ npm run test:functional
```
