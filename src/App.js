import React from "react";
import { ThemeProvider } from "styled-components";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import theme from "./styles/theme";
import Header from "./components/Header";
import GlobalStyle from "./components/GlobalStyle";
import Proposal from './pages/Proposal';
import Approval from './pages/Approval';
import Routes from './consts/Routes';

class App extends React.Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <React.Fragment>
          <GlobalStyle />
          <Header />
          <Router>
            <Switch>
              <Route path={Routes.PROPOSAL} component={Proposal} exact />
              <Route path={Routes.APPROVAL} component={Approval} exact />
            </Switch>
          </Router>
        </React.Fragment>
      </ThemeProvider>
    );
  }
}

export default App;
