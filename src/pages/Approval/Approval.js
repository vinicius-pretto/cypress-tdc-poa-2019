import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  max-width: 800px;
  margin: auto;
`

const Title = styled.h1`
  font-size: 2em;
  margin-top: 20px;
  margin-bottom: 20px;
`

const Gif = styled.img`
  display: block;
  width: 100%;
  margin: auto;
`

const Approval = () => {
  return (
    <Container>
      <Title>Aprovado, você é o bichão mesmo hein doido!</Title>
      <Gif src="https://media.giphy.com/media/m2Q7FEc0bEr4I/giphy.gif" alt="Brent Rambo GIF" />
    </Container>
  );
};

export default Approval;
