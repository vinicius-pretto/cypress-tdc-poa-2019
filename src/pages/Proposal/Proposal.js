import React from 'react';
import { Formik } from "formik";
import styled from "styled-components";

import Container from "../../components/Container";
import Input from "../../components/Input";
import TextArea from "../../components/TextArea";
import Select from "../../components/Select";
import Button from "../../components/Button";
import { tracks } from './tracks.json';
import validationSchema from './validationSchema';
import TalkService from '../../services/TalkService';
import Routes from '../../consts/Routes';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const Form = styled.form`
  margin-top: 20px;
  margin-bottom: 20px;
`;

class Proposal extends React.Component {
  initialValues = {
    title: '',
    summary: '',
    messageForCommitte: '',
    language: '',
    type: '',
    track: ''
  }

  onSubmit = talk => {
    TalkService.submitTalk(talk)
      .then(() => {
        this.props.history.replace(Routes.APPROVAL);
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    return (
      <Container>
        <Formik 
          initialValues={this.initialValues} 
          onSubmit={this.onSubmit}
          validationSchema={validationSchema}
        >
          {props => {
            const {
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit
            } = props;

            return (
              <Form onSubmit={handleSubmit}>
                <Input
                  id="title"
                  type="text"
                  label="Título da apresentação"
                  required
                  value={values.title}
                  error={errors.title}
                  touched={touched.title}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <TextArea
                  id="summary"
                  label="Resumo da sua apresentação, para o público"
                  cols={60}
                  rows={6}
                  required
                  value={values.summary}
                  error={errors.summary}
                  touched={touched.summary}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <TextArea
                  id="messageForCommitte"
                  label="Mensagem para os coordenadores da trilha, motivação para sua apresentação"
                  cols={60}
                  rows={6}
                  required
                  value={values.messageForCommitte}
                  error={errors.messageForCommitte}
                  touched={touched.messageForCommitte}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <Select
                  id="language"
                  name="language"
                  value={values.language}
                  label="Idioma"
                  options={[
                    { name: "--- Selecione o idioma ---", value: "" },
                    { name: "Português", value: "Português" },
                    { name: "English", value: "English" }
                  ]}
                  required
                  error={errors.language}
                  touched={touched.language}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <Select
                  id="type"
                  name="type"
                  value={values.type}
                  label="Tipo de palestra"
                  options={[
                    { name: "--- Selecione o tipo de palestra ---", value: "" },
                    { name: "Palestra ou debate (50min)", value: "talk" },
                    { name: "Palestra curta (25min)", value: "short-talk" },
                    { name: "Palestra mini (15min)", value: "mini-talk" }
                  ]}
                  required
                  error={errors.type}
                  touched={touched.type}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <Select
                  id="track"
                  name="track"
                  value={values.track}
                  label="Trilha da sua proposta"
                  options={tracks}
                  required
                  error={errors.track}
                  touched={touched.track}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <ButtonContainer>
                  <Button data-cy="submit-button" type="submit">Enviar</Button>
                </ButtonContainer>
              </Form>
            );
          }}
        </Formik>
      </Container>
    );
  }
}

export default Proposal;
