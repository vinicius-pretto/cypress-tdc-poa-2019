import axios from 'axios';
import config from '../config';

const submitTalk = talk => {
  return axios.post(`${config.talkServiceUrl}/talks`, talk);
}

export default {
  submitTalk
}
