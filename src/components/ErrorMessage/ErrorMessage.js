import React from 'react';
import styled from 'styled-components';

const ErrorMessageStyled = styled.p`
  font-size: 14px;
  color: ${props => props.theme.colors.red[900]};
  margin-top: 5px;
`

const ErrorMessage = ({ children }) => {
  return (
    <ErrorMessageStyled>{children}</ErrorMessageStyled>
  )
}

export default ErrorMessage;
