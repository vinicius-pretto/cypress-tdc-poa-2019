import styled from 'styled-components';

import Device from '../../styles/Device';

const Container = styled.div`
  max-width: 1170px;
  margin-left: auto;
  margin-right: auto;

  @media ${Device.LAPTOP_EXTRA_SMALL} {
    max-width: 970px;
    padding-left: 15px;
    padding-right: 15px;
  }

  @media ${Device.TABLET} {
    max-width: 750px;
    padding-left: 15px;
    padding-right: 15px;
  }
  
  @media ${Device.MOBILE} {
    padding-left: 15px;
    padding-right: 15px;
  }
`

export default Container;
