import ProposalPage from '../support/ProposalPage';

describe('Submit a talk', () => {
  let proposalPage;

  beforeEach(() => {
    proposalPage = new ProposalPage();
  });

  it('should submit a talk', () => {
    proposalPage
      .visit()
      .mockTalkServiceRequest()
      .fillForm()
      .submitTalk();
  });
});
