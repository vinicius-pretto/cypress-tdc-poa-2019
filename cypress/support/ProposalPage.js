import HttpStatus from 'http-status';

import Routes from '../../src/consts/Routes';
import talk from '../fixtures/talk.json';

class ProposalPage {
  visit() {
    cy.visit(Routes.PROPOSAL);
    return this;
  }

  mockTalkServiceRequest() {
    cy.server();
    cy.route({
      url: '**/talks',
      method: 'POST',
      status: HttpStatus.CREATED,
      response: {
        id: '157d6c39-117d-411d-b57a-435dda8408ab'
      }
    });
    return this;
  }

  fillForm() {
    cy.get('[data-cy=title]').type(talk.title);
    cy.get('[data-cy=summary]').type(talk.summary);
    cy.get('[data-cy=messageForCommitte]').type(talk.messageForCommitte);
    cy.get('[data-cy=language]').select(talk.language);
    cy.get('[data-cy=type]').select(talk.type);
    cy.get('[data-cy=track]').select(talk.track);
    return this;
  }

  submitTalk() {
    cy.get('[data-cy=submit-button]').click();
    return this;
  }
}

export default ProposalPage;
